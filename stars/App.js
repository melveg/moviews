import React, {Component} from 'react'

class Movie extends React.Component{
 render(){
  return {
   <div class="movie medium card blue-grey darken-1">
   <div class="card-image waves-effect waves-block waves-light">
    <img class="activator" src="static/media/{this.props.poster}">
   </div>
   <div class="card-content white-text">
           <span class="card-title activator">{{ movie.film_name }}<i class="material-icons right">. . .</i></span>
   <div class="critical" id="critical-{{ movie.id }}"><canvas id="canvas" width=80 height=20></canvas><span class="rating"></span></div>
    <div class="card-action"><a href="/movie/{{ movie.id }}">GO TO DETAILS</a></div>
   </div>
   <div class="card-reveal">
    <span class="card-title grey-text text-darken-4">{{ movie.film_name }}<i class="material-icons right">X</i></span>
    <p>{{ movie.synopsis|truncatechars:200 }}</p>
   </div>
  </div>
  };
 }
}
