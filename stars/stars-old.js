var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

function drawStar(cx, cy, spikes, outerRadius, innerRadius) {
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var step = Math.PI / spikes;

    ctx.strokeSyle = "#000";
    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y)
        rot += step

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y)
        rot += step
    }
    ctx.lineTo(cx, cy - outerRadius)
    ctx.lineWidth=1;
    ctx.strokeStyle='#e24329';
    ctx.stroke();
    ctx.fillStyle='#ffffff';
    ctx.fill();

}

function drawRect(w,h){
ctx.globalCompositeOperation="source-atop";
 ctx.beginPath();
 ctx.rect(0,0,w,h);
 ctx.fillStyle="#fc6d26";
 ctx.fill();
}

drawStar(10, 10, 5, 6, 3);
drawStar(25, 10, 5, 6, 3);
drawStar(40, 10, 5, 6, 3);
drawStar(55, 10, 5, 6, 3);
drawStar(70, 10, 5, 6, 3);
drawRect(40,20);
