import React, {Component} from 'react';
import starsbar from './stars.js'
import {Link} from 'react-router-dom'
import axios from 'axios'

var starBarReview = null;

class ReviewCreator extends React.Component{
 constructor(props){
  super(props);
  this.state = {
   username:'',
   password:'',
   review:'',
   auth:((sessionStorage.getItem('token')!==null && sessionStorage.getItem('token')!==undefined)?true:false),
   message:'',
   review:'',
  };
  this.handleInputChange = this.handleInputChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
 }
 handleInputChange(event) {
  this.setState({
   [event.target.name]:event.target.value
  });
 }
 handleSubmit(event){
  //login form processing
  if(event.target.name=="login"){
   axios.post('http://localhost/api/api-token-auth/',{
    username:this.state.username,
    password:this.state.password
   }).
   then((response)=>{
    //console.log(response);
    if(response.data.token!==null && response.data.token!==undefined){
     sessionStorage.setItem('token',response.data.token); 
     this.setState({auth:true});
    }else{
     this.setState({message:'Credentials are not valid'}); 
    }
   }).catch((error)=> {
    this.setState({message:'Credentials are not valid'});
   });
  }else{
   //process the review
   //console.log(starBarReview.stars+' '+this.state.review+' '+this.props.movieid);
   if(starBarReview.stars!=0 && this.state.review!=''){
    //console.log(sessionStorage.getItem('token'));
    axios.post('http://localhost/api/review',{
     stars:starBarReview.stars,
     review:this.state.review,
     movieid:this.props.movieid
    },{
     headers:{
      "Authorization" : 'Token '+sessionStorage.getItem('token')
     }
    }).then((response)=>{
     if(response.data[0].indexOf('Error')==-1)
      this.setState({message:response.data[0]}); 
      window.location.reload();
    }).catch((error)=> {
     this.setState({message:error});
    });
   }else{
    this.setState({message:"You have to select stars or write something first"});
   }
  }
  event.preventDefault();
 }
 componentDidMount(){
  //this.setState({auth:((sessionStorage.getItem('token')!==null && sessionStorage.getItem('token')!==undefined)?true:false)});
  if(this.state.auth==true){
   starBarReview = new starsbar('critical-review');
   starBarReview.roundStar = true;
   starBarReview.drawbar(); 
  }
 }
 render(){
  //if exists token , user authenticated so.. render review form
  if(this.state.auth){
   return (
    <div class="row">
     <div class="col s12">
      <form name="register-review" onSubmit={this.handleSubmit}>
       <div class="row">                                                                                               <div class="col s16">                                                                                           <span class="orange-text">{this.state.message}</span>                                                         </div>                                                                                                        </div>
       <div class="row">
        <div class="input-field col s6">
	 <b class="critical-label">Select star:</b>
	 <div className="critical" id='critical-review'><canvas id="canvas" width="80" height="20"></canvas><span className="rating"></span></div>
	</div>
        <div class="input-field col s12">
	 <textarea id="review" name="review" class="materialize-textarea" onChange={this.handleInputChange}></textarea>
	 <label for="review">Enter Review</label>
        </div>
       </div>
       <div class="row">
        <div class="col s6">
         <button class="btn waves-effect waves-light" type="submit" name="action">Send Review</button>
        </div>
       </div>
      </form>
     </div>
    </div>
   );
  }else{
  //user not authenticated. Render auth form
   return (
    <div class="row">
     <div class="col s12">
      <form name="login" onSubmit={this.handleSubmit}>
       <div class="row">
        <div class="col s16">
	 <span class="orange-text">{this.state.message}</span> 
	</div>
       </div>
       <div class="row">
        <div class="input-field col s6">
         <input id="username" name="username" value={this.state.username} type="text" onChange={this.handleInputChange} class="white-text"/>
         <label for="username">Enter Username</label>
        </div>
       </div>
       <div class="row">
        <div class="input-field col s6">
         <input id="password" name="password" value={this.state.password} type="password" onChange={this.handleInputChange} class="white-text"/>
	 <label for="password">Enter Password</label>
        </div>
       </div>
       <div class="row">
        <div class="col s6">
         <button class="btn waves-effect waves-light" type="submit" name="action">Login</button>
        </div>
       </div>
      </form>
     </div>
    </div>
   );
  }
 }
}

class Review extends React.Component{
 constructor(props){
  super(props);
  this.starReview = [];
 }
 componentDidMount(){
  this.starReview[this.props.id] = new starsbar('review-'+this.props.id);
  this.starReview[this.props.id].saved = true;
  this.starReview[this.props.id].roundStar = false;
  this.starReview[this.props.id].stars = (this.props.stars!==null&&this.props.stars!==undefined)?this.props.stars:0;
  this.starReview[this.props.id].selectedBar = ((this.starReview[this.props.id].stars*80)/5).toFixed(2);
  this.starReview[this.props.id].canvas.click();
 }
 render(){
  return(
   <div class="review">
    <div class="divider"></div>
    <div class="section">
     <div className="review" id={ "review-"+this.props.id }><canvas id="canvas" width="80" height="20"></canvas><span className="rating"></span></div>
     <p>{this.props.comment}</p>
    </div>
   </div>
  );
 }
}

class Movie extends React.Component{
 constructor(props){
   super(props);
   this.state = {
    id:null,
    template:'card'
   };
 }
 componentDidMount(){
  if(this.props.match!==null && this.props.match!==undefined){
   if(this.props.match.params.id!==null && this.props.match.params.id!==undefined){
    this.state.template ='detail';
    axios.get('http://localhost/api/movies/'+this.props.match.params.id).
    then((response)=>{
     //this.setState({movies:JSON.parse(response.data.movies)});
     var movie = JSON.parse(response.data.movie);
     var reviews = movie[1];
     movie = movie[0];
     this.setState({id:movie[0], poster:movie[7], film_name:movie[1], synopsis:movie[6],rating:movie[8], reviews: reviews},()=>{
      var starBar = new starsbar('critical-'+this.state.id);
      starBar.saved = true;
      starBar.roundStar = false;
      starBar.stars = (this.state.rating!==null&&this.state.rating!==undefined)?this.state.rating:0;
      starBar.selectedBar = ((starBar.stars*80)/5).toFixed(2);
      starBar.canvas.click();
     });
    });
   }
  }else{
    this.setState({id:this.props.id, poster:this.props.poster, film_name:this.props.film_name, synopsis:this.props.synopsis,rating:this.props.rating},()=>{
    var starBar = [];
    starBar[this.state.id] = new starsbar('critical-'+this.state.id);
    starBar[this.state.id].saved = true;
    starBar[this.state.id].roundStar = false;
    starBar[this.state.id].stars = (this.state.rating!==null&&this.state.rating!==undefined)?this.state.rating:0;
    starBar[this.state.id].selectedBar = ((starBar[this.state.id].stars*80)/5).toFixed(2);
    starBar[this.state.id].canvas.click();
    });
  }
 }
 
 render(){
    if(this.state.template=="detail"){
     return(
      <div className="movie large card blue-grey darken-1">
       <div className="card-image waves-effect waves-block waves-light">
        <img src={"http://localhost/static/media/"+this.state.poster}/>
       </div>
        <div class="card-content white-text">
	 <span class="card-title">{ this.state.film_name }</span>
	 <div className="critical overall" id={ "critical-"+this.state.id }><span>Overall rating: </span><canvas id="canvas" width="80" height="20"></canvas><span className="rating"></span></div>
	 <p class="before-synopsis">{this.state.synopsis}</p>
	 <span class="left">Make your review:</span>
	 <ReviewCreator 
	 movieid={ this.state.id } 
	 />
	 <span class="left">Reviews:</span>
	 { this.state.reviews!=null && this.state.reviews!=undefined && this.state.reviews.map((review)=>
	  <Review id={review[0]}
	          stars={review[1]}
		  comment={review[2]}/>
	 )}
	</div>
	<div className="card-action"><Link to="/">&lt; Back</Link></div>
      </div>
     );
    }else{
    return (
     <div className="movie medium card blue-grey darken-1">
      <div className="card-image waves-effect waves-block waves-light">
       <img className="activator" src={"http://localhost/static/media/"+this.state.poster}/>
      </div>
      <div className="card-content white-text">
       <span className="card-title activator">{ this.state.film_name }<i className="material-icons right">. . .</i></span>
       <div className="critical" id={ "critical-"+this.state.id }><canvas id="canvas" width="80" height="20"></canvas><span className="rating"></span></div>
       <div className="card-action"><Link to="/">HOME</Link><Link to={"/movie/"+this.state.id}>DETAILS</Link></div>
      </div>
      <div className="card-reveal">
       <span className="card-title grey-text text-darken-4">{ this.state.film_name }<i className="material-icons right">X</i></span>
       <p className="grey-text">{ this.state.synopsis }</p>
      </div>
     </div>
    );
   }
 }
}

/*
module.exports = {
 Uno:Uno,
 Dos:Dos
}
*/

export default Movie;
