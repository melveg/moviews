function  starsbar(id){
 var self = this;
 //var canvas = document.getElementById(id);
 var critical = document.getElementById(id);
 this.canvas = critical.getElementsByTagName("canvas")[0];
 var canvas = this.canvas;
 var ctx = canvas.getContext("2d");
 var saved = false;
 this.roundStar = false;
 this.selectedBar = 0;
 this.stars = 0;
 var starblock = {1:19,2:33,3:47,4:63,5:80};
 function drawStar(xi,yi,orad,irad){
  // rotate each 270
  var rot = (Math.PI/2)*3;
  var x = xi;
  var y = yi;
  var step = Math.PI / 5;
  ctx.strokeSyle = "#000";
  ctx.beginPath();
  ctx.moveTo(xi, yi - orad);
  for (var i = 0; i < 5; i++) {
 		 x = xi + Math.cos(rot) * orad;
     y = yi + Math.sin(rot) * orad;
     ctx.lineTo(x, y);
     rot += step;

     x = xi + Math.cos(rot) * irad;
     y = yi + Math.sin(rot) * irad;
     ctx.lineTo(x, y);
     rot += step;
  }
  ctx.lineTo(xi, yi - orad)
  ctx.lineWidth=1;
  ctx.strokeStyle='#e24329';
  ctx.stroke();
  ctx.fillStyle='#ffffff';
  ctx.fill();
 }

 function drawRect(w,h){
  ctx.globalCompositeOperation="source-atop";
  ctx.beginPath();
  ctx.rect(0,0,w,h);
  ctx.fillStyle="#fc6d26";
  ctx.fill();
 }

 function drawbar(bar){
  //ctx.save();
  //ctx.setTransform(1, 0, 0, 1, 0, 0);
  //ctx.clearRect(0, 0, canvas.width, canvas.height);
  //ctx.restore();
  drawStar(10, 10, 6, 3);
  drawStar(25, 10, 6, 3);
  drawStar(40, 10, 6, 3);
  drawStar(55, 10, 6, 3);
  drawStar(70, 10, 6, 3);
  drawRect((bar!==null && bar!==undefined)?bar:'0',20);
 }

 this.drawbar = drawbar;

 canvas.addEventListener('mousemove',function(evt){
  if(!saved){
   var rect = canvas.getBoundingClientRect();
   /*drawStar(10, 10, 6, 3);
   drawStar(25, 10, 6, 3);
   drawStar(40, 10, 6, 3);
   drawStar(55, 10, 6, 3);
   drawStar(70, 10, 6, 3);
   drawRect(evt.clientX-rect.left,20);*/
   self.selectedBar = evt.clientX-rect.left;
   //var rating = ((self.selectedBar<=4)?1:((self.selectedBar>76)?80:self.selectedBar));
   self.stars = ((5*self.selectedBar)/80).toFixed(2);
   //var stars =Math.ceil(((5*selectedBar)/80).toFixed(2));
   //var starblock = {1:19,2:33,3:47,4:63,5:80};
   if(!self.roundStar)
    drawbar(evt.clientX-rect.left);
   else{
    self.stars = Math.ceil(self.stars);
    drawbar(starblock[self.stars]);
   }
   critical.getElementsByTagName('span')[0].innerHTML = self.stars;
  }
 });

 canvas.addEventListener('click',function(evt){
  if(!self.roundStar)
   drawbar(self.selectedBar);
  else
   drawbar(starblock[self.stars]);
  saved = true;
 });

}

export default starsbar;
