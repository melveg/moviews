//External Libraries
import React, {Component} from 'react';
import axios from 'axios'
import {
Switch,
Route,
Link,
useParams
} from "react-router-dom";

//App resources
import logo from './logo.svg';
import './App.css';

import Movie from './Movie'

class App extends React.Component{
 constructor(props){
  super(props);
  this.state = {
   movies:null
  };
 }
 componentDidMount(){
  axios.get('http://localhost/api/movies').
   then((response)=>{
    //console.log(JSON.parse(response.data.movies));
    this.setState({movies:JSON.parse(response.data.movies)});
   });
 }
 render() {
  return (
   <div className="App">
    <div className="App-header">
     <div className="row">
      <div className="col offset-s3 s6 offset-s3">
       <Switch>
        <Route exact path="/">
          { this.state.movies!=null && this.state.movies!=undefined && this.state.movies.map((movie)=>
	   <Movie key={movie[0]}
               id={movie[0]}
	       poster={movie[7]}
	       film_name={movie[1]}
	       synopsis={movie[6]}
	       rating={movie[8]}/>
          )}
        </Route>
        <Route exact path="/movie/:id" component={Movie}></Route>
       </Switch>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default App;
