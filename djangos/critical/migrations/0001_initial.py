# Generated by Django 2.0.13 on 2019-10-23 04:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('film_name', models.CharField(max_length=200)),
                ('poster', models.ImageField(upload_to='')),
                ('release_date', models.DateField()),
                ('classification', models.CharField(max_length=200)),
                ('genre', models.CharField(max_length=400)),
                ('duration', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stars', models.SmallIntegerField()),
                ('comment', models.TextField(max_length=800)),
                ('movie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='critical.Movie')),
            ],
        ),
    ]
