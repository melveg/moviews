from django.shortcuts import render

# Create your views here.
from .models import Movie, Review

def index(request):
    #movies = Movie.objects.order_by('-release_date')
    movies = Movie.objects.raw('select m.*,sum(r.stars)*1.0/count(r.id) as rating from critical_movie m left join critical_review r on (m.id=r.movie_id) group by m.id order by m.release_date, rating desc')
    context = {'movies': movies}
    #template = loader.get_template('movies/index.html')
    #return HttpResponse(template.render(context, request))
    return render(request, 'movies/index.html', context)

def detail(request,movie_id):
    movie = Movie.objects.get(id=movie_id)
    context = {'movie': movie}
    return render(request, 'movies/detail.html', context)
