from django.db import models

# Create your models here.

class Movie(models.Model):
	film_name = models.CharField(max_length=200)
	poster = models.ImageField(upload_to = "critical/static/media")
	release_date = models.DateField()
	classification = models.CharField(max_length=200)
	genre = models.CharField(max_length=400)
	duration = models.TimeField()
	synopsis = models.TextField(max_length=800, null=True)
	def __str__(self):
		return self.film_name

class Review(models.Model):
	movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
	user = models.CharField(max_length=200, default="test")
	stars = models.SmallIntegerField()
	comment = models.TextField(max_length=800)
	def __str__(self):
		return self.comment[0:50] + "..."
