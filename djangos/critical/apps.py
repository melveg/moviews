from django.apps import AppConfig


class CriticalConfig(AppConfig):
    name = 'critical'
