from django.urls import path
from . import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
 path('api/hello',views.HelloWorld.as_view(), name='hello'),
 path('api/movies',views.Movies.as_view(), name='movies'),
 path('api/movies/<int:movie_id>',views.Movies.as_view(), name='movies'),
 path('api/review',views.Reviews.as_view(), name='review'),
 path('api/api-token-auth/', obtain_auth_token, name='api_token_auth'),
];
