from django.shortcuts import render
from django.core import serializers
from django.db import connection
import json

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response

#Define movies and reviews models
from critical.models import Movie, Review

#Allow just for certain classes
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import AllowAny

@permission_classes((AllowAny, ))
class HelloWorld(APIView):
      def get(self, request):
          return Response({'message','Hello People, Welcome to the movies review api'})
@permission_classes((AllowAny, ))
class Movies(APIView):
      #def post(self,request)
      def get(self, request, movie_id=None):
          if(movie_id!=None):
              cursor = connection.cursor()
              cursor.execute('select m.id, m.film_name, m.release_date, m.classification, m.genre, m.duration, m.synopsis, m.poster,sum(r.stars)*1.0/count(r.id) as rating from critical_movie m left join critical_review r on (m.id=r.movie_id) where m.id='+str(movie_id)+' group by m.id order by m.release_date, rating desc')
              movie = cursor.fetchall()
              cursor.execute('select * from critical_review r where r.movie_id='+str(movie_id));
              reviews = cursor.fetchall();
              return Response({'movie':json.dumps([movie[0],reviews], sort_keys=True, default=str)}, content_type='application/json')
          else:
              cursor = connection.cursor()
              cursor.execute('select m.id, m.film_name, m.release_date, m.classification, m.genre, m.duration, m.synopsis, m.poster,sum(r.stars)*1.0/count(r.id) as rating from critical_movie m left join critical_review r on (m.id=r.movie_id) group by m.id order by m.release_date, rating desc')
              #movies = Movie.objects.raw('select m.*,sum(r.stars)*1.0/count(r.id) as rating from critical_movie m left join critical_review r on (m.id=r.movie_id) group by m.id order by m.release_date, rating desc')
              movies = cursor.fetchall()
              return Response({'movies':json.dumps(movies, sort_keys=True, default=str)}, content_type='application/json')
              #return Response({'movies':serializers.serialize('json',movies)})
class Reviews(APIView):
      def post(self,request):
          req = json.loads(request.body.decode('utf-8'))
          review = Review(movie_id=req['movieid'], stars=req['stars'], comment=req['review'], user='test')
          review.save()
          if(review.id!=None):
             return Response({'message','Your review have been saved successfully.'})
          else:
             return Response({'message','Error. There was a problem saving you review. Try later'})
